// IMPORT LIBRARIES
var http = require('http');
var url = require('url');
var fs = require('fs');

// CONSTANTS
// represents the number of field in array. Jist to do this peace of code more readeble
var CAT = 1,
    POST = 2;

// SERVER
//expected url in form '/:cat/:post"
http.createServer(function (request, response) {
    // get path url and split it to "fields" array
    var path = url.parse(request.url).pathname.split('/');
    // after splitting we should have 3 field, look up
    if (path.length !== 3) {
        response.writeHead(404);
        response.write("Bad URL. Bad life. Nothing good. If u think that it's issue, please p.m. me @yurii.leso (ig)");
        response.end();
        return
    }

    // in case of url is correct ...
    fs.readFile("index.html", function (error, data) {
        // caching errors
        if (error) {
            response.writeHead(404);
            response.write(error);
            response.end();
        } else {
            // responsing
            response.writeHead(200, {
                'Content-Type': 'text/html'
            });
            response.write(
                data
                    .toString()
                    .replace(/__CAT/g, path[CAT])
                    .replace(/__POST/g, path[POST])
            );
            response.end();
        }
    });
}).listen(process.env.PORT || 5000);
