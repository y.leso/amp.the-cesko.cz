<?php
/// Author: Leso Yurii <yuriy@lesyo.eu>
/// (c) Aug 31 2019


// get actual path. Split it by '/' into array
$PATH = explode('/', $_SERVER['PHP_SELF']);

if (sizeof($PATH) <= 3)
    return "This server is used to render AMP pages for the-cesko publication. 
    Url should be in format //amp.the-cesko.cz/\<CAT_URL\>/\<POST_URL\>. 
    You have visited bad url. If u think it's okay, please contact me y.leso@the-cesko.cz";

$CAT = $PATH[2];
$POST = $PATH[3];

// open template and replace value's to own (bothcat and post urls)
$ampTemplate = fopen("index.html", "r") or die("There some problems with server. Please contact me as soon as possible (y.leso@the-cesko.cz)");
echo str_replace(
    ['__CAT', '__POST'],
    [$CAT, $POST],
    fread($ampTemplate, filesize("index.html"))
);
fclose($ampTemplate);

/// (c) Copyright Mark Limited Group, s.r.o.
/// Using in not allowed.
